package com.stemmildt.evaluate.domain.`object`

import com.stemmildt.evaluate.domain.model.product.`object`.Product
import com.stemmildt.evaluate.domain.model.product.`object`.ProductDescription
import com.stemmildt.evaluate.domain.model.product.`object`.ProductId
import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.assertions.isEqualTo
import strikt.assertions.isTrue

class ProductTest {

  @Test
  fun expectToCalculateEqualityOnlyById() {
    val product = Product.of(ProductId(1), ProductDescription("Description"))
    val sameProduct = Product.of(ProductId(1), ProductDescription("Other description"))

    expectThat(product.isValid)
      .isTrue()
    expectThat(sameProduct.isValid)
      .isTrue()
    expectThat(product)
      .isEqualTo(sameProduct)
  }
}

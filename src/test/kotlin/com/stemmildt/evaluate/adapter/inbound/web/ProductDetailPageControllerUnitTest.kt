package com.stemmildt.evaluate.adapter.inbound.web

import com.stemmildt.evaluate.adapter.inbound.InboundAdapterConfiguration.EVALUATE_PATH
import com.stemmildt.evaluate.adapter.inbound.web.ProductDetailPageController.Companion.PRODUCTS_PATH
import com.stemmildt.evaluate.application.port.inbound.ProductApplicationService
import com.stemmildt.evaluate.domain.model.product.`object`.Product
import com.stemmildt.evaluate.domain.model.product.`object`.ProductDescription
import com.stemmildt.evaluate.domain.model.product.`object`.ProductId
import gg.jte.ContentType
import gg.jte.TemplateEngine
import io.mockk.mockk
import org.junit.jupiter.api.Test
import org.springframework.http.MediaType.TEXT_HTML
import org.springframework.test.web.reactive.server.WebTestClient
import strikt.api.expectThat
import strikt.assertions.contains

class ProductDetailPageControllerUnitTest {

  private val productApplicationServiceMock = mockk<ProductApplicationService>()
  private val productDetailPageController = ProductDetailPageController(TemplateEngine.createPrecompiled(ContentType.Html), productApplicationServiceMock)

  private val webTestClient = WebTestClient.bindToRouterFunction(productDetailPageController.routes()).build()

  @Test
  fun expectToGetProductDetailPage() {
    val productDescriptionValue = "Pizza"

    val pizza = Product(ProductId(1), ProductDescription(productDescriptionValue))
    // TODO NOW
//    every { runBlocking { productApplicationServiceMock.loadProductCount() } } returns 1L
//    every { runBlocking { productApplicationServiceMock.loadAllProducts() } } returns listOf(pizza.validNel()).toFlux().asFlow()

    webTestClient.get()
      .uri(EVALUATE_PATH + PRODUCTS_PATH)
      .accept(TEXT_HTML)
      .exchange()
      .expectStatus().isOk
      .expectBody(String::class.java)
      .value {
        expectThat(it)
          .contains(productDescriptionValue)
      }
  }
}

package com.stemmildt.evaluate.adapter.inbound.web

import arrow.core.andThen
import com.stemmildt.evaluate.IntegrationTest
import com.stemmildt.evaluate.IntegrationTest.Companion.serviceContext
import com.stemmildt.evaluate.IntegrationTest.Companion.webTestClient
import com.stemmildt.evaluate.adapter.inbound.InboundAdapterConfiguration.EVALUATE_PATH
import com.stemmildt.evaluate.adapter.inbound.web.ProductDetailPageController.Companion.PRODUCTS_PATH
import com.stemmildt.evaluate.application.ProductApplicationService
import com.stemmildt.evaluate.domain.model.messaging.ProductSavedEvent
import com.stemmildt.evaluate.domain.model.product.`object`.Product
import com.stemmildt.evaluate.domain.model.product.`object`.ProductDescription
import com.stemmildt.evaluate.domain.model.product.`object`.ProductId
import com.stemmildt.extensions.Validation.validate
import org.junit.jupiter.api.Test
import org.springframework.http.MediaType.TEXT_HTML
import strikt.api.expectThat
import strikt.assertions.contains

class ProductDetailPageControllerIntegrationTest : IntegrationTest {

  private val productApplicationService = serviceContext.getBean(ProductApplicationService::class.java)

  @Test
  fun expectToGetProductDetailPage() {
    val productDescriptionValue = "Pizza"

    validate(
      ProductId.of(1),
      ProductDescription.of(productDescriptionValue)
    ).andThen { (id, description) ->
      Product.of(id, description).andThen(ProductSavedEvent::of)
    }.tap {
      // TODO NOW
      // runBlocking { productApplicationService.publishProductSavedEvent(it) }
    }

    webTestClient.get()
      .uri(EVALUATE_PATH + PRODUCTS_PATH)
      .accept(TEXT_HTML)
      .exchange()
      .expectStatus().isOk
      .expectBody(String::class.java)
      .value {
        expectThat(it)
          .contains(productDescriptionValue)
      }
  }
}

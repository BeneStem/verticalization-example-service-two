package com.stemmildt.evaluate

import com.stemmildt.evaluate.EvaluateServiceConfiguration.evaluateServiceApplication
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.BeforeAll
import org.springframework.context.ConfigurableApplicationContext
import org.springframework.test.web.reactive.server.WebTestClient
import org.testcontainers.containers.KafkaContainer
import org.testcontainers.containers.MongoDBContainer
import org.testcontainers.utility.DockerImageName

interface IntegrationTest {

  companion object {
    internal lateinit var serviceContext: ConfigurableApplicationContext

    private val testPort = (10000..10500).random()

    val webTestClient = WebTestClient.bindToServer()
      .baseUrl("http://localhost:$testPort")
      .build()

    private val KAFKA_CONTAINER = KafkaContainer(DockerImageName.parse("confluentinc/cp-kafka:7.3.1-1-ubi8"))
    private val MONGODB_CONTAINER = MongoDBContainer(DockerImageName.parse("mongo:6.0.3-focal"))

    @JvmStatic
    @BeforeAll
    fun beforeAll() {
      KAFKA_CONTAINER.start()
      MONGODB_CONTAINER.start()
      serviceContext = evaluateServiceApplication.run(
        arrayOf(
          "--spring.kafka.bootstrap-servers=${KAFKA_CONTAINER.bootstrapServers}",
          "--spring.data.mongodb.uri=${MONGODB_CONTAINER.replicaSetUrl}",
          "--server.port=$testPort"
        ),
        "test"
      )
    }

    @JvmStatic
    @AfterAll
    fun afterAll() {
      serviceContext.close()
      MONGODB_CONTAINER.stop()
      KAFKA_CONTAINER.stop()
    }
  }
}

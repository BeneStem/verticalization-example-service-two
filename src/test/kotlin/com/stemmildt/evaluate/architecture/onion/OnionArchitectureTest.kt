package com.stemmildt.evaluate.architecture.onion

import com.tngtech.archunit.core.importer.ImportOption.DoNotIncludeArchives
import com.tngtech.archunit.core.importer.ImportOption.DoNotIncludeTests
import com.tngtech.archunit.junit.AnalyzeClasses
import com.tngtech.archunit.junit.ArchTest
import com.tngtech.archunit.library.Architectures.OnionArchitecture
import com.tngtech.archunit.library.Architectures.onionArchitecture

@AnalyzeClasses(
  packages = ["com.stemmildt.evaluate"],
  importOptions = [DoNotIncludeTests::class, DoNotIncludeArchives::class]
)
class OnionArchitectureTest {

  // TODO NOW fix this one

  @ArchTest
  val onionArchitectureIsRespected: OnionArchitecture =
    onionArchitecture()
      .domainModels("..domain.model..")
      .domainServices("..domain.service..")
      .applicationServices("..application..")
      .adapter("inbound", "..adapter.inbound..")
      .adapter("outbound", "..adapter.outbound..")
}

package com.stemmildt.evaluate.architecture

import com.tngtech.archunit.core.importer.ImportOption.DoNotIncludeArchives
import com.tngtech.archunit.core.importer.ImportOption.DoNotIncludeTests
import com.tngtech.archunit.junit.AnalyzeClasses
import com.tngtech.archunit.junit.ArchTest
import com.tngtech.archunit.lang.ArchRule
import com.tngtech.archunit.library.DependencyRules.NO_CLASSES_SHOULD_DEPEND_UPPER_PACKAGES
import com.tngtech.archunit.library.dependencies.SliceRule
import com.tngtech.archunit.library.dependencies.SlicesRuleDefinition.slices

@AnalyzeClasses(
  packages = ["com.stemmildt.evaluate"],
  importOptions = [DoNotIncludeTests::class, DoNotIncludeArchives::class]
)
class DependencyRulesTest {

  // TODO NOW fix this one

  @ArchTest
  val adapterSlices: SliceRule =
    slices()
      .matching("..product.adapter.(*)..")
      .should().beFreeOfCycles()

  @ArchTest
  val applicationSlices: SliceRule =
    slices()
      .matching("..product.application.(*)..")
      .should().beFreeOfCycles()

  @ArchTest
  val noClassesShouldDependUpperPackages: ArchRule = NO_CLASSES_SHOULD_DEPEND_UPPER_PACKAGES
}

package com.stemmildt.util

import gg.jte.output.StringOutput

object TextEventStream {

  private fun toTextEventStreamData(text: String) = text.lines()
    .filter { it.trim().isNotEmpty() }
    .joinToString("\n") { "data: $it" }
    .let { "$it\n\n" }

  fun StringOutput.toTextEventStreamDataString() = toTextEventStreamData(this.toString())
}

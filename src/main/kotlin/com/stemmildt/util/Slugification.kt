package com.stemmildt.util

import com.github.slugify.Slugify
import java.util.Locale

object Slugification {

  private val slugify = Slugify.builder().locale(Locale.GERMAN).build()

  private fun slugifyString(text: String): String = slugify.slugify(text)

  fun String.slugify() = slugifyString(this)
}

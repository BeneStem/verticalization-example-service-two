package com.stemmildt.evaluate

import com.stemmildt.evaluate.EvaluateServiceConfiguration.evaluateServiceApplication

object EvaluateService

fun main(args: Array<String>) {
  evaluateServiceApplication.run(args)
}

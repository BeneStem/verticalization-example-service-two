package com.stemmildt.evaluate.adapter.inbound.messaging

import org.apache.kafka.clients.consumer.ConsumerConfig.AUTO_OFFSET_RESET_CONFIG
import org.apache.kafka.clients.consumer.ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG
import org.apache.kafka.clients.consumer.ConsumerConfig.GROUP_ID_CONFIG
import org.apache.kafka.clients.consumer.ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG
import org.apache.kafka.clients.consumer.ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG
import org.apache.kafka.common.serialization.ByteArrayDeserializer
import org.apache.kafka.common.serialization.StringDeserializer
import org.springframework.boot.autoconfigure.kafka.KafkaProperties
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory
import org.springframework.kafka.core.DefaultKafkaConsumerFactory

object KafkaConsumerConfiguration {

  const val PRODUCTS_TOPIC = "products"

  fun listenerFactory(kafkaProperties: KafkaProperties) =
    ConcurrentKafkaListenerContainerFactory<String, ByteArray>().also {
      it.consumerFactory = DefaultKafkaConsumerFactory(
        mapOf(
          BOOTSTRAP_SERVERS_CONFIG to kafkaProperties.bootstrapServers,
          GROUP_ID_CONFIG to kafkaProperties.consumer.groupId,
          AUTO_OFFSET_RESET_CONFIG to kafkaProperties.consumer.autoOffsetReset,
          KEY_DESERIALIZER_CLASS_CONFIG to StringDeserializer::class.java,
          VALUE_DESERIALIZER_CLASS_CONFIG to ByteArrayDeserializer::class.java
        )
      )
    }
}

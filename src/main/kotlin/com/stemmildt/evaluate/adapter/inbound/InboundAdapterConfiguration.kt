package com.stemmildt.evaluate.adapter.inbound

import com.stemmildt.evaluate.adapter.inbound.messaging.KafkaConsumerConfiguration
import com.stemmildt.evaluate.adapter.inbound.messaging.KafkaProductSavedEventListener
import com.stemmildt.evaluate.adapter.inbound.web.JteConfiguration
import com.stemmildt.evaluate.adapter.inbound.web.ProductDetailPageController
import org.springframework.boot.autoconfigure.kafka.KafkaProperties
import org.springframework.core.env.getRequiredProperty
import org.springframework.fu.kofu.configuration
import org.springframework.fu.kofu.webflux.webFlux

object InboundAdapterConfiguration {

  const val EVALUATE_PATH: String = "/evaluate"

  operator fun invoke() = configuration {
    enable(webConfiguration())
    enable(messagingConfiguration())
  }

  private fun webConfiguration() = configuration {
    webFlux {
      port = env.getRequiredProperty<Int>("server.port")
      codecs {
        string()
      }
    }
    beans {
      bean(JteConfiguration::templateEngine)

      bean<ProductDetailPageController>()
      bean(ProductDetailPageController::routes)
    }
  }

  private fun messagingConfiguration() = configuration {
    beans {
      configurationProperties<KafkaProperties>(prefix = "spring.kafka")
      bean<KafkaConsumerConfiguration>()
      bean(KafkaConsumerConfiguration::listenerFactory)
      bean<KafkaProductSavedEventListener>()
      bean(KafkaProductSavedEventListener::create)
    }
  }
}

package com.stemmildt.evaluate.adapter.inbound.web

import com.stemmildt.evaluate.adapter.inbound.InboundAdapterConfiguration.EVALUATE_PATH
import com.stemmildt.evaluate.application.port.inbound.ProductApplicationService
import com.stemmildt.evaluate.domain.model.product.`object`.ProductId
import com.stemmildt.evaluate.domain.model.web.ProductDetail
import com.stemmildt.extensions.EntityResponse.buildAndAwait
import com.stemmildt.extensions.EntityResponse.ok
import gg.jte.Content
import gg.jte.TemplateEngine
import gg.jte.output.StringOutput
import org.springframework.http.MediaType.TEXT_HTML
import org.springframework.web.reactive.function.server.EntityResponse
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.coRouter

class ProductDetailPageController(
  private val templateEngine: TemplateEngine,
  private val productApplicationService: ProductApplicationService
) {

  companion object {

    const val PRODUCTS_PATH: String = "/products"
  }

  fun routes() = coRouter {
    accept(TEXT_HTML).nest {
      GET("$EVALUATE_PATH$PRODUCTS_PATH/{id}", ::getProductDetailPage)
    }
  }

  // TODO NOW this does not show up 404 page correctly

  private suspend fun getProductDetailPage(serverRequest: ServerRequest): EntityResponse<String> =
    StringOutput().let {
      findProductDetail(serverRequest.pathVariable("id").toIntOrNull()).let { productDetail ->
        templateEngine.render(
          "templates/pageFrame",
          mapOf(
            "head" to Content { templateEngine.render("fragments/head", mapOf("title" to productDetail.headline), it) },
            "main" to Content { templateEngine.render("fragments/productDetail", mapOf("productDetail" to productDetail), it) }
          ),
          it
        )
        ok(it.toString()).contentType(TEXT_HTML).buildAndAwait()
      }
    }

  private suspend fun findProductDetail(id: Int?) =
    id?.let {
      ProductId.of(it).fold(
        { ProductDetail.InvalidId(id) },
        { findProductDetail(it) }
      )
    } ?: ProductDetail.InvalidId(id)

  private suspend fun findProductDetail(validatedId: ProductId) =
    productApplicationService.loadProductById(validatedId).fold(
      { ProductDetail.Error() },
      {
        it.fold(
          { ProductDetail.NotFound(validatedId) },
          { ProductDetail.Ok(it) }
        )
      }
    )
}

package com.stemmildt.evaluate.adapter.inbound.messaging.model

import arrow.core.andThen
import com.stemmildt.create.v1.KafkaProduct
import com.stemmildt.evaluate.domain.model.messaging.ProductSavedEvent
import com.stemmildt.evaluate.domain.model.product.`object`.Product
import com.stemmildt.evaluate.domain.model.product.`object`.ProductDescription
import com.stemmildt.evaluate.domain.model.product.`object`.ProductId
import com.stemmildt.extensions.Validation.validate

object KafkaProductEvents {

  fun KafkaProduct.Saved.toProductSavedEvent() =
    validate(ProductId.of(id), ProductDescription.of(description))
      .andThen { (id, description) -> Product.of(id, description) }
      .andThen { ProductSavedEvent.of(it) }
}

package com.stemmildt.evaluate.adapter.outbound

import com.stemmildt.evaluate.adapter.outbound.persistence.mongo.MongoProductRepository
import org.springframework.boot.autoconfigure.mongo.MongoProperties
import org.springframework.core.env.getProperty
import org.springframework.fu.kofu.configuration
import org.springframework.fu.kofu.mongo.reactiveMongodb

object OutboundAdapterConfiguration {

  operator fun invoke() = configuration {
    enable(persistenceConfiguration())
  }

  private fun persistenceConfiguration() = configuration {
    reactiveMongodb {
      uri = env.getProperty<String>("spring.data.mongodb.uri") ?: MongoProperties.DEFAULT_URI
    }
    beans {
      bean<MongoProductRepository>()
    }
  }
}

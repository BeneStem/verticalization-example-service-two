package com.stemmildt.evaluate.adapter.outbound.persistence.mongo

import arrow.core.Option
import arrow.core.invalidNel
import com.stemmildt.evaluate.adapter.outbound.persistence.mongo.model.MongoProduct
import com.stemmildt.evaluate.adapter.outbound.persistence.mongo.model.MongoProduct.Companion.toMongoProduct
import com.stemmildt.evaluate.adapter.outbound.persistence.mongo.model.MongoProductError
import com.stemmildt.evaluate.application.port.outbound.ProductRepository
import com.stemmildt.evaluate.domain.model.product.`object`.Product
import com.stemmildt.evaluate.domain.model.product.`object`.ProductId
import org.springframework.dao.DuplicateKeyException
import org.springframework.data.mongodb.core.ReactiveFluentMongoOperations
import org.springframework.data.mongodb.core.awaitOneOrNull
import org.springframework.data.mongodb.core.insert
import org.springframework.data.mongodb.core.oneAndAwait
import org.springframework.data.mongodb.core.query
import org.springframework.data.mongodb.core.query.Criteria.where
import org.springframework.data.mongodb.core.query.Query.query
import org.springframework.data.mongodb.core.query.isEqualTo

class MongoProductRepository(
  private val mongo: ReactiveFluentMongoOperations
) : ProductRepository {

  override suspend fun findProductById(id: ProductId) =
    Option.fromNullable(
      mongo.query<MongoProduct>()
        .matching(query(where(MongoProduct::id.name).isEqualTo(id.value.toString())))
        .awaitOneOrNull()
        ?.toProduct()
    )

  override suspend fun persistProduct(product: Product) =
    try {
      mongo.insert<MongoProduct>()
        .oneAndAwait(product.toMongoProduct())
        .toProduct()
    } catch (duplicateKeyException: DuplicateKeyException) {
      MongoProductError.DuplicateKey.invalidNel()
    }
}

package com.stemmildt.evaluate.domain.model.web

import com.stemmildt.evaluate.domain.model.product.`object`.Product
import com.stemmildt.evaluate.domain.model.product.`object`.ProductId
import org.jmolecules.ddd.annotation.AggregateRoot

@AggregateRoot
sealed class ProductDetail private constructor(val headline: String, val text: String) {

  class Ok private constructor(headline: String, text: String) : ProductDetail(headline, text) {

    companion object {

      operator fun invoke(product: Product) = Ok(product.description.value, product.id.value.toString())
    }
  }

  class Error private constructor(headline: String, text: String) : ProductDetail(headline, text) {

    companion object {

      operator fun invoke() = Error("Found product has errors", "")
    }
  }

  class InvalidId private constructor(headline: String, text: String) : ProductDetail(headline, text) {

    companion object {

      operator fun invoke(id: Int?) = InvalidId("Invalid productId", id.toString())
    }
  }

  class NotFound private constructor(headline: String, text: String) : ProductDetail(headline, text) {

    companion object {

      operator fun invoke(id: ProductId) = NotFound("Product not found", id.value.toString())
    }
  }

  override fun equals(other: Any?): Boolean {
    if (this === other) return true
    if (other !is ProductDetail) return false

    if (headline != other.headline) return false
    if (text != other.text) return false

    return true
  }

  override fun hashCode(): Int {
    var result = headline.hashCode()
    result = 31 * result + text.hashCode()
    return result
  }

  override fun toString() = "ProductDetailPage(headline='$headline', text='$text')"
}

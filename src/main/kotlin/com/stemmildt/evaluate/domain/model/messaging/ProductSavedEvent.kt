package com.stemmildt.evaluate.domain.model.messaging

import arrow.core.validNel
import com.stemmildt.evaluate.domain.model.product.`object`.Product
import org.jmolecules.ddd.annotation.AggregateRoot

// TODO NOW this object should probably also get an own id

@AggregateRoot
class ProductSavedEvent private constructor(val product: Product) {

  companion object {

    fun of(product: Product) =
      ProductSavedEvent(product).validNel() // more validation maybe later
  }

  override fun equals(other: Any?): Boolean {
    if (this === other) return true
    if (other !is ProductSavedEvent) return false

    if (product != other.product) return false

    return true
  }

  override fun hashCode() = product.hashCode()

  override fun toString() = "ProductSavedEvent(product=$product)"
}

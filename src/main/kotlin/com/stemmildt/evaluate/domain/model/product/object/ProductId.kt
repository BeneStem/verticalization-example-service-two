package com.stemmildt.evaluate.domain.model.product.`object`

import arrow.core.ValidatedNel
import arrow.core.invalidNel
import arrow.core.validNel
import com.stemmildt.evaluate.domain.model.product.`object`.ProductError.IdInvalid
import com.stemmildt.extensions.Validation.ValidationException
import com.stemmildt.extensions.Validation.get
import org.jmolecules.ddd.annotation.ValueObject

@ValueObject
@JvmInline
value class ProductId private constructor(val value: Int) {

  companion object {

    @Throws(ValidationException::class)
    operator fun invoke(value: Int) =
      of(value).get()

    fun of(value: Int): ValidatedNel<ProductError, ProductId> =
      if (value in 1..99999998) {
        ProductId(value).validNel()
      } else {
        IdInvalid.invalidNel()
      }
  }
}

package com.stemmildt.evaluate.domain.model.product.`object`

import com.stemmildt.util.Error

sealed class ProductError private constructor() : Error {

  data object IdInvalid : ProductError()
  data object DescriptionInvalid : ProductError()
}

package com.stemmildt.evaluate.application

import com.stemmildt.evaluate.application.port.inbound.ProductApplicationService
import com.stemmildt.evaluate.application.port.outbound.ProductRepository
import com.stemmildt.evaluate.domain.model.product.`object`.ProductId

class ProductApplicationService(
  private val productRepository: ProductRepository
) : ProductApplicationService {

  override suspend fun loadProductById(id: ProductId) =
    productRepository.findProductById(id)
}

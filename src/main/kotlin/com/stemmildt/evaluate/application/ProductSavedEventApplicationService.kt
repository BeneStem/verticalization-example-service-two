package com.stemmildt.evaluate.application

import com.stemmildt.evaluate.application.port.inbound.ProductSavedEventApplicationService
import com.stemmildt.evaluate.application.port.outbound.ProductRepository
import com.stemmildt.evaluate.domain.model.messaging.ProductSavedEvent
import kotlinx.coroutines.runBlocking

class ProductSavedEventApplicationService(
  private val productRepository: ProductRepository
) : ProductSavedEventApplicationService {

  override fun handle(productSavedEvent: ProductSavedEvent) =
    runBlocking {
      productRepository.persistProduct(productSavedEvent.product)
      Unit
    }
}

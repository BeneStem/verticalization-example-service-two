package com.stemmildt.evaluate.application.port.inbound

import com.stemmildt.evaluate.domain.model.messaging.ProductSavedEvent

interface ProductSavedEventApplicationService {

  fun handle(productSavedEvent: ProductSavedEvent)
}

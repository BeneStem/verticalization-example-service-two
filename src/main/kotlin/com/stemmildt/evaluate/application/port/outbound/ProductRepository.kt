package com.stemmildt.evaluate.application.port.outbound

import arrow.core.Option
import arrow.core.ValidatedNel
import com.stemmildt.evaluate.domain.model.product.`object`.Product
import com.stemmildt.evaluate.domain.model.product.`object`.ProductId
import com.stemmildt.util.Error

interface ProductRepository {

  suspend fun findProductById(id: ProductId): Option<ValidatedNel<Error, Product>>

  suspend fun persistProduct(product: Product): ValidatedNel<Error, Product>
}

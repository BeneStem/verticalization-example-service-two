package com.stemmildt.evaluate.application.port.inbound

import arrow.core.Option
import arrow.core.ValidatedNel
import com.stemmildt.evaluate.domain.model.product.`object`.Product
import com.stemmildt.evaluate.domain.model.product.`object`.ProductId
import com.stemmildt.util.Error

interface ProductApplicationService {

  suspend fun loadProductById(id: ProductId): Option<ValidatedNel<Error, Product>>
}

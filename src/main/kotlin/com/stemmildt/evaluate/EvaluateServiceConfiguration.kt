package com.stemmildt.evaluate

import com.stemmildt.evaluate.adapter.inbound.InboundAdapterConfiguration
import com.stemmildt.evaluate.adapter.outbound.OutboundAdapterConfiguration
import com.stemmildt.evaluate.application.ApplicationConfiguration
import org.springframework.fu.kofu.reactiveWebApplication

object EvaluateServiceConfiguration {

  val evaluateServiceApplication = reactiveWebApplication {
    enable(InboundAdapterConfiguration())
    enable(OutboundAdapterConfiguration())
    enable(ApplicationConfiguration())
  }
}

@file:Suppress("LongParameterList")

package com.stemmildt.extensions

import arrow.core.Tuple10
import arrow.core.Tuple4
import arrow.core.Tuple5
import arrow.core.Tuple6
import arrow.core.Tuple7
import arrow.core.Tuple8
import arrow.core.Tuple9
import arrow.core.Validated
import arrow.core.ValidatedNel
import arrow.core.valueOr
import arrow.core.zip

object Validation {

  fun <Z, A, B> validate(
    a: ValidatedNel<Z, A>,
    b: ValidatedNel<Z, B>
  ): ValidatedNel<Z, Pair<A, B>> = a.zip(b) { validA, validB ->
    Pair(validA, validB)
  }

  fun <Z, A, B, C> validate(
    a: ValidatedNel<Z, A>,
    b: ValidatedNel<Z, B>,
    c: ValidatedNel<Z, C>
  ): ValidatedNel<Z, Triple<A, B, C>> = a.zip(b, c) { validA, validB, validC ->
    Triple(validA, validB, validC)
  }

  fun <Z, A, B, C, D> validate(
    a: ValidatedNel<Z, A>,
    b: ValidatedNel<Z, B>,
    c: ValidatedNel<Z, C>,
    d: ValidatedNel<Z, D>
  ): ValidatedNel<Z, Tuple4<A, B, C, D>> = a.zip(b, c, d) { validA, validB, validC, validD ->
    Tuple4(validA, validB, validC, validD)
  }

  fun <Z, A, B, C, D, E> validate(
    a: ValidatedNel<Z, A>,
    b: ValidatedNel<Z, B>,
    c: ValidatedNel<Z, C>,
    d: ValidatedNel<Z, D>,
    e: ValidatedNel<Z, E>
  ): ValidatedNel<Z, Tuple5<A, B, C, D, E>> = a.zip(b, c, d, e) { validA, validB, validC, validD, validE ->
    Tuple5(validA, validB, validC, validD, validE)
  }

  fun <Z, A, B, C, D, E, F> validate(
    a: ValidatedNel<Z, A>,
    b: ValidatedNel<Z, B>,
    c: ValidatedNel<Z, C>,
    d: ValidatedNel<Z, D>,
    e: ValidatedNel<Z, E>,
    f: ValidatedNel<Z, F>
  ): ValidatedNel<Z, Tuple6<A, B, C, D, E, F>> = a.zip(b, c, d, e, f) { validA, validB, validC, validD, validE, validF ->
    Tuple6(validA, validB, validC, validD, validE, validF)
  }

  fun <Z, A, B, C, D, E, F, G> validate(
    a: ValidatedNel<Z, A>,
    b: ValidatedNel<Z, B>,
    c: ValidatedNel<Z, C>,
    d: ValidatedNel<Z, D>,
    e: ValidatedNel<Z, E>,
    f: ValidatedNel<Z, F>,
    g: ValidatedNel<Z, G>
  ): ValidatedNel<Z, Tuple7<A, B, C, D, E, F, G>> = a.zip(b, c, d, e, f, g) { validA, validB, validC, validD, validE, validF, validG ->
    Tuple7(validA, validB, validC, validD, validE, validF, validG)
  }

  fun <Z, A, B, C, D, E, F, G, H> validate(
    a: ValidatedNel<Z, A>,
    b: ValidatedNel<Z, B>,
    c: ValidatedNel<Z, C>,
    d: ValidatedNel<Z, D>,
    e: ValidatedNel<Z, E>,
    f: ValidatedNel<Z, F>,
    g: ValidatedNel<Z, G>,
    h: ValidatedNel<Z, H>
  ): ValidatedNel<Z, Tuple8<A, B, C, D, E, F, G, H>> = a.zip(b, c, d, e, f, g, h) { validA, validB, validC, validD, validE, validF, validG, validH ->
    Tuple8(validA, validB, validC, validD, validE, validF, validG, validH)
  }

  fun <Z, A, B, C, D, E, F, G, H, I> validate(
    a: ValidatedNel<Z, A>,
    b: ValidatedNel<Z, B>,
    c: ValidatedNel<Z, C>,
    d: ValidatedNel<Z, D>,
    e: ValidatedNel<Z, E>,
    f: ValidatedNel<Z, F>,
    g: ValidatedNel<Z, G>,
    h: ValidatedNel<Z, H>,
    i: ValidatedNel<Z, I>
  ): ValidatedNel<Z, Tuple9<A, B, C, D, E, F, G, H, I>> = a.zip(b, c, d, e, f, g, h, i) { validA, validB, validC, validD, validE, validF, validG, validH, validI ->
    Tuple9(validA, validB, validC, validD, validE, validF, validG, validH, validI)
  }

  fun <Z, A, B, C, D, E, F, G, H, I, J> validate(
    a: ValidatedNel<Z, A>,
    b: ValidatedNel<Z, B>,
    c: ValidatedNel<Z, C>,
    d: ValidatedNel<Z, D>,
    e: ValidatedNel<Z, E>,
    f: ValidatedNel<Z, F>,
    g: ValidatedNel<Z, G>,
    h: ValidatedNel<Z, H>,
    i: ValidatedNel<Z, I>,
    j: ValidatedNel<Z, J>
  ): ValidatedNel<Z, Tuple10<A, B, C, D, E, F, G, H, I, J>> = a.zip(b, c, d, e, f, g, h, i, j) { validA, validB, validC, validD, validE, validF, validG, validH, validI, validJ ->
    Tuple10(validA, validB, validC, validD, validE, validF, validG, validH, validI, validJ)
  }

  class ValidationException(message: String, val error: Any) : IllegalArgumentException(message)

  @Throws(ValidationException::class)
  fun <E, A> Validated<E, A>.get(): A = valueOr { throw ValidationException("Validated expected to be valid", it as Any) }
}
